#!/usr/bin/env bash
file=resultsp3pr.log
wFile=warmupp3pr.log
nthreads=4
if [ -f "$file" ] && [ -f "$wFile" ]
then
  rm $file $wFile
fi
touch $file $wFile
./warmup.sh >> $wFile # warmup
# echo "non-auto block_size" >> $file
echo "seq" >> $file
./build/seq $dim >> $file
for i in `seq 1 1 100`;
do
  echo "iter $i:" >> $file # iteration
  for dim in 4;
  do
    ./build/omp $dim $(( $dim/$nthreads )) >> $file # results dumped
    sleep 1
  done
done
