#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#ifdef OMP
#include <omp.h>
#define SCH static
#endif

#define RAND rand() % 100
#define DIM 4
#define DEBUG 1
#define PRINT 0

void init_mat_sup (int dim, float *M);
void init_mat_inf (int dim, float *M);
void matmul (float *A, float *B, float *C, int dim, int block_size);
void matmul_sup (float *A, float *B, float *C, int dim, int block_size);
void matmul_inf (float *A, float *B, float *C, int dim, int block_size);
void print_matrix (float *M, int dim);

//Variables to measure execution time
struct timeval tIni, tFin;
double tExe;

/* Usage: ./matmul <dim> [block_size]*/

int main (int argc, char* argv[])
{
	gettimeofday(&tIni, NULL);
	int block_size = 250, dim = DIM;
	float *A, *B, *C;

	if (argc > 1) dim = atoi (argv[1]);
	if (argc == 3) block_size = atoi (argv[2]);

	printf("dim: %d block_size: %d\n", dim, block_size);

	A = (float *)malloc(dim * dim * sizeof(float));
	B = (float *)malloc(dim * dim * sizeof(float));
	C = (float *)malloc(dim * dim * sizeof(float));

	#ifdef OMP
	printf("omp execution\n");
	#else
	printf("seq execution\n");
	#endif

	init_mat_sup(dim, A);
	init_mat_inf(dim, B);
	matmul(A, B, C, dim, block_size);
	//matmul_sup(A, B, C, dim, block_size);
	//matmul_inf(A, B, C, dim, block_size);

	#if PRINT
	//print_matrix(A, dim);
	//print_matrix(B, dim);
	print_matrix(C, dim);
	#endif

	free(A);
	free(B);
	free(C);

	gettimeofday(&tFin, NULL);
  tExe = (tFin.tv_sec-tIni.tv_sec)+(tFin.tv_usec-tIni.tv_usec)/1000000.0;
  printf("execution time: %lf s\n", tExe);

	exit (0);
}

void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

void matmul (float *A, float *B, float *C, int dim, int block_size)
{
	int i, j, k;
	double init;
	#ifdef OMP
	#pragma omp parallel private(i,j,k) shared(dim, A, B, C, block_size)
	{

	#if DEBUG
	#pragma omp master
	{
		printf("omp_num_threads: %d\n", omp_get_num_threads());
	}
	#endif
	printf("start\n");
	init = omp_get_wtime();
	//#pragma omp for collapse(2) schedule(SCH)
	#pragma omp for collapse(2) schedule(SCH, block_size)
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	#pragma omp for collapse(3) schedule(SCH)
	//#pragma omp for collapse(3) schedule(SCH, block_size)
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			for (k=0; k < dim; k++){
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
				printf("thread %d (i,j,k)=(%d,%d,%d)\n", omp_get_thread_num(),i,j,k);
				//sleep(1);
			}
	}	//end parallel region
	printf("end %f\n", omp_get_wtime()-init);

	#else	//SEQ
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			for (k=0; k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	#endif
}

void matmul_sup (float *A, float *B, float *C, int dim, int block_size)
{
	int i, j, k;

	#ifdef OMP
	#pragma omp parallel private(i,j,k) shared(dim, A, B, C, block_size)
	{

	#if DEBUG
	#pragma omp master
	{
		printf("omp_num_threads: %d\n", omp_get_num_threads());
	}
	#endif

	#pragma omp for collapse(2) schedule(SCH)
	//#pragma omp for collapse(2) schedule(SCH, block_size)
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	#pragma omp for nowait collapse(2) schedule(SCH)
	//#pragma omp for collapse(2) schedule(SCH, block_size)
	for (i=0; i < (dim-1); i++)
		for (j=0; j < (dim-1); j++)
			for (k=(i+1); k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}	//end parallel region

	#else	//SEQ
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	for (i=0; i < (dim-1); i++)
		for (j=0; j < (dim-1); j++)
			for (k=(i+1); k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	#endif
}

void matmul_inf (float *A, float *B, float *C, int dim, int block_size)
{
	int i, j, k;

	#ifdef OMP
	#pragma omp parallel private(i,j,k) shared(dim, A, B, C, block_size)
	{

	#if DEBUG
	#pragma omp master
	{
		printf("omp_num_threads: %d\n", omp_get_num_threads());
	}
	#endif

	#pragma omp for nowait collapse(2) schedule(SCH)
	//#pragma omp for collapse(2) schedule(SCH, block_size)
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	#pragma omp for nowait collapse(2) schedule(SCH)
	//#pragma omp for collapse(2) schedule(SCH, block_size)
	for (i=1; i < dim; i++)
		for (j=1; j < dim; j++)
			for (k=0; k < i; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}	//end parallel region

	#else	//SEQ
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
		for (j=1; j < dim; j++)
			for (k=0; k < i; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	#endif

}

void print_matrix (float *M, int dim)
{
	int i,j;

	for(i=0; i<dim; i++){
		for(j=0; j<dim; j++)
			printf("%.f\t", M[i*dim+j]);
		printf("\n");
	}
	printf("\n");
}
