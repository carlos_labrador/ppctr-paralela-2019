# P4: Filtrado de un vídeo mediante tareas OpenMP

#### Carlos Labrador Viñuela

#### Diciembre 2019

## Prefacio

Objetivos conseguidos

Paralelización de un programa secuencial de filtrado de video usando OpenMP.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.
~~~bash
$ inxi -F
System:    Host: dartz-HP Kernel: 5.0.0-36-generic x86_64 bits: 64 Desktop: Gnome 3.28.4
           Distro: Ubuntu 18.04.3 LTS
Machine:   Device: laptop System: HP product: HP ENVY Notebook v: Type1ProductConfigId serial: N/A
           Mobo: HP model: 80DF v: 87.39 serial: N/A UEFI: Insyde v: F.19 date: 09/15/2015
Battery    BAT1: charge: 18.4 Wh 50.9% condition: 36.1/45.0 Wh (80%)
CPU:       Dual core Intel Core i5-6200U (-MT-MCP-) cache: 3072 KB
           clock speeds: max: 2800 MHz 1: 500 MHz 2: 500 MHz 3: 500 MHz 4: 500 MHz
Graphics:  Card: Intel Skylake GT2 [HD Graphics 520]
           Display Server: x11 (X.Org 1.20.4 ) driver: i915 Resolution: 1920x1080@60.05hz
           OpenGL: renderer: Mesa DRI Intel HD Graphics 520 (Skylake GT2) version: 4.5 Mesa 19.0.8
Audio:     Card Intel Sunrise Point-LP HD Audio driver: snd_hda_intel
           Sound: Advanced Linux Sound Architecture v: k5.0.0-36-generic
Network:   Card: Intel Wireless 7265 driver: iwlwifi
           IF: wlp1s0 state: up mac: 4c:34:88:5b:6a:4e
Drives:    HDD Total Size: 128.0GB (9.3% used)
           ID-1: /dev/sda model: SAMSUNG_MZNLF128 size: 128.0GB
Partition: ID-1: / size: 19G used: 11G (62%) fs: ext4 dev: /dev/sda6
           ID-2: swap-1 size: 0.50GB used: 0.00GB (0%) fs: swap dev: /dev/sda5
RAID:      No RAID devices: /proc/mdstat, md_mod kernel module present
Sensors:   System Temperatures: cpu: 37.0C mobo: N/A
           Fan Speeds (in rpm): cpu: N/A
Info:      Processes: 260 Uptime: 37 min Memory: 1904.3/3840.8MB Client: Shell (bash) inxi: 2.3.56
$ lscpu
Arquitectura:                        x86_64
modo(s) de operación de las CPUs:    32-bit, 64-bit
Orden de los bytes:                  Little Endian
CPU(s):                              4
Lista de la(s) CPU(s) en línea:      0-3
Hilo(s) de procesamiento por núcleo: 2
Núcleo(s) por «socket»:              2
«Socket(s)»                          1
Modo(s) NUMA:                        1
ID de fabricante:                    GenuineIntel
Familia de CPU:                      6
Modelo:                              78
Nombre del modelo:                   Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz
Revisión:                            3
CPU MHz:                             2745.224
CPU MHz máx.:                        2800,0000
CPU MHz mín.:                        400,0000
BogoMIPS:                            4800.00
Virtualización:                      VT-x
Caché L1d:                           32K
Caché L1i:                           32K
Caché L2:                            256K
Caché L3:                            3072K
CPU(s) del nodo NUMA 0:              0-3
Indicadores:                         fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d
~~~

## 2. Diseño e Implementación del Software

En el fichero `video_task_omp.c` tenemos la implementación paralela del programa secuencial inicialmente dado. La paralelización se ha realizado en la parte principal del programa, mediante tareas que ejecutan `fgauss`.  

~~~c++
#pragma omp parallel shared(pixels, filtered, height, width, seq) private(i)
{
 #pragma omp master
 {
 i = 0;
 int j;
 do
 {
   size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);
   if (size)
   {
     #pragma omp task
     {
       fgauss(pixels[i], filtered[i], height, width);
     }
   }

   if(feof(in)){
     #pragma omp taskwait
     for (j = 0; j<i; j++) {
       fwrite(filtered[j], (height+2) * (width + 2) * sizeof(int), 1, out);
     }
   }

   if(i==seq-1){
     #pragma omp taskwait
     for (j = 0; j<=i; j++) {
       fwrite(filtered[j], (height+2) * (width + 2) * sizeof(int), 1, out);
     }
     i=0;
   }else{
     i++;
   }
 }while (!feof(in));
 }
}//end parallel region
~~~

En lugar de escribir cada vez que se llame a `fgauss` como en la parte secuencial, escribimos cada `seq` veces. Si se acaba el fichero sin haber llegado a `seq`, escribimos hasta donde se haya llegado.  

## 3. Metodología y desarrollo de las pruebas realizadas

Se usan varios scripts con el fin de probar el programa. Además de los scripts de ejemplo dados, algo modificados, se ha creado un nuevo script **warmup**, cuya finalidad es hacer el calentamiento, ejecutando el programa unas cuantas veces. Guardamos los resultados del calentamiento en otro archivo, por si se desea comprobarlos. Los resultados de la prueba son guardados en un archivo.

###### Condiciones de la ejecución de las pruebas:
  - Misma frecuencia fijada en todos los procesadores, en este caso, la máxima.
  - Los scripts se ejecutan en la bash, la cual es la única aplicación abierta durante la ejecución de las pruebas.
  - Los scripts lanzan varias veces las dos versiones del programa (secuencial, y OMP) obteniendo los tiempos de ejecución de la ROI, en segundos.

Scripts usados para el benchmarking:

**benchmark.sh:**

```sh
#!/usr/bin/env bash
file=resultsp4.log
wFile=warmupp4.log
if [ -f "$file" ] && [ -f "$wFile" ]
then
  rm $file $wFile
fi
touch $file $wFile
echo "start warmup"
./warmup.sh >> $wFile # warmup
echo "end warmup"
for season in 1 2 3; do
  echo "start season $season"
  for benchcase in seq omp; do
    echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      # printf "$i:" >> $file # iteration
      ./benchsuite.sh $benchcase >> $file # results dumped
      sleep 1
    done
  done
  echo "end season $season"
done
```

**benchsuite.sh:**

```sh
#!/usr/bin/env bash
case "$1" in
  seq)
    ./build/video_task
    ;;
  omp)
    for nthreads in 1 2 4 8; do
      echo "omp $nthreads"
      THREADS=$nthreads ./build/video_task_omp
      diff movie.out movie_omp.out
    done
    ;;
esac
```

**warmup.sh:**

```sh
#!/usr/bin/env bash
for i in `seq 1 1 3`; do
  ./build/video_task
  THREADS=4 ./build/video_task_omp
  diff movie.out movie_omp.out
done
sleep 1
```

#### Análisis de los resultados

![imagen_1](images/imagen_1.png)

- Se utiliza el `generator.c` original, sin modificar.
- Observamos que con 2 hilos es suficiente, ya que a partir de ahí el crecimiento del speedup es mínimo.

El speedup es calculado mediante la media de las ejecuciones realizadas.
  - seq = 4,2554095 s
  - omp
    - 1 thread = 3,345863167 s
    - 2 threads = 1,782216767 s
    - 4 threads = 1,772510567 s
    - 8 threads = 1,75943733 s
    - 12 threads = 1,758378867 s

##### Incidencia
Destacar una incidencia encontrada en el benchmarking. Originalmente, como se muestra en los scripts, la idea era pasar el número de hilos como parámetro para automatizar las pruebas, en este caso como variable de entorno pero también se ha probado como argumento normal. Este valor se guardaba en una variable `nthreads`. Se ha detectado que a la hora de establecer los hilos con la directiva `num_threads` de OpenMP, el programa pierde rendimiento (tarda más, por ejemplo con 4 hilos pasa de 1,77 s de media a 2,1 s) pasándole el valor en dentro de una variable (`#pragma omp parallel num_threads(nthreads)`) que pasándole el valor directamente (`#pragma omp parallel num_threads(4)`). Con el fin de encontrar el mejor rendimiento, para conseguir los datos finales se ha modificado el método para el benchmarking, asignando manualmente (modificando el archivo .c y compilando cada vez) el número de hilos y entonces ejecutar un script que lance el programa las veces que sean necesarias.

Cuando no se utiliza esta directiva, OpenMP asigna correctamente el número de hilos (en mi caso 4) y no hay pérdida de rendimiento.

## 4. Discusión

El tiempo que se usa en el benchmarking es el **tiempo de ejecución de la ROI**, usando la función de omp `omp_get_wtime`.

### 1. Funcionalidad añadida/modificada sobre el código original.
El programa hace lo mismo de manera paralela, mediante tareas. Además ya no se escriben los pixels filtrados cada vez que se llama a `fgauss`, sino cada `seq` veces.

### 2. Explicar brevemente la función de cada directiva de OpenMP usada.
- `parallel`: Crea la región paralela.      
- `master`: El trabajo de la sección de esta directiva lo realiza el hilo principal.
- `task`: Crea las tareas, las cuales realizan el trabajo que contengan en su scope.
- `taskwait`: Esperamos a que las tareas acaben su trabajo para continuar.

### 3. Etiquetar las variables y explicar el por qué de la etiqueta.
- `shared`: pixels, filtered, height, width, seq
- `private`: i

### 4. Explica cómo se consigue el paralelismo en el main.
La paralelización se consigue creando tasks que van ejecutando `fgauss`, mientras se sigue ejecutando el programa principal. Cuando se hayan ejecutado `seq` veces, esperamos a que terminen las tasks para escribirlas en el fichero de salida.

### 5. Speedup conseguido con la paralelización.
Se ha llegado a conseguir un speedup de `2,42`, como se ha podido observar en la sección anterior.

### 6. ¿Optimización sobre la función `fgauss`?
Se puede paralelizar la función, con un `#pragma omp for collapse(2)` por ejemplo, pero no optimizaría el rendimiento. Si paralelizamos la función que está siendo realizada por tasks previamente creadas, no se aprovecha el paralelismo, dado que los cores están ocupados por las tasks y no pueden ser usados para ejecutar la función de manera paralela.
