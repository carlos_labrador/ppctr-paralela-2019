#!/usr/bin/env bash
for i in `seq 1 1 3`; do
  ./build/video_task
  ./build/video_task_omp
  diff movie.out movie_omp.out
done
sleep 1
