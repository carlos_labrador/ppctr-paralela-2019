#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>

void fgauss (int *, int *, long, long);

int main(int argc, char *argv[]) {

  FILE *in;
  FILE *out;
  int i, j, size, seq = 8;
  int **pixels, **filtered;
  int nthreads = 4;

  if (argc == 2) seq = atoi (argv[1]);

  //chdir("/tmp");
  in = fopen("movie.in", "rb");
  if (in == NULL) {
    perror("movie.in");
    exit(EXIT_FAILURE);
  }

  out = fopen("movie_omp.out", "wb");
  if (out == NULL) {
    perror("movie_omp.out");
    exit(EXIT_FAILURE);
  }

  long width, height;

  fread(&width, sizeof(width), 1, in);
  fread(&height, sizeof(height), 1, in);

  fwrite(&width, sizeof(width), 1, out);
  fwrite(&height, sizeof(height), 1, out);

  pixels = (int **) malloc (seq * sizeof (int *));
  filtered = (int **) malloc (seq * sizeof (int *));

  for (i=0; i<seq; i++)
  {
    pixels[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
    filtered[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
  }

  double tIni = omp_get_wtime();
  #pragma omp parallel shared(pixels, filtered, height, width, seq) private(i)
  {
   #pragma omp master
   {
   i = 0;
   int j;
   do
   {
     size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);
     if (size)
     {
       #pragma omp task
       {
         fgauss(pixels[i], filtered[i], height, width);
       }
     }

     if(feof(in)){
       #pragma omp taskwait
       for (j = 0; j<i; j++) {
         fwrite(filtered[j], (height+2) * (width + 2) * sizeof(int), 1, out);
       }
     }

     if(i==seq-1){
       #pragma omp taskwait
       for (j = 0; j<=i; j++) {
         fwrite(filtered[j], (height+2) * (width + 2) * sizeof(int), 1, out);
       }
       i=0;
     }else{
       i++;
     }
   }while (!feof(in));
   }
  }//end parallel region
  double tFin = omp_get_wtime();

  for (i=0; i<seq; i++)
  {
    free (pixels[i]);
    free (filtered[i]);
  }
  free(pixels);
  free(filtered);

  fclose(out);
  fclose(in);

  printf("ROI execution time: %f\n", tFin-tIni);

  return EXIT_SUCCESS;
}

void fgauss (int *pixels, int *filtered, long height, long width)
{
	int y, x, dx, dy;
	int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
	int sum;

  //#pragma omp parallel
  //{
    //#pragma omp for collapse(2)
    for (x = 0; x < width; x++) {
      for (y = 0; y < height; y++)
      {
        sum = 0;
        for (dx = 0; dx < 5; dx++)
          for (dy = 0; dy < 5; dy++)
            if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < height))
              sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
        filtered[x*height+y] = (int) sum/273;
      }
    }
  //}
}
