#!/usr/bin/env bash
case "$1" in
  seq)
    ./build/video_task
    ;;
  omp)
    for nthreads in 1 2 4 8; do
      echo "omp $nthreads"
      ./build/video_task_omp 8 $nthreads
      diff movie.out movie_omp.out
    done
    ;;
esac
