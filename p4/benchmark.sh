#!/usr/bin/env bash
file=resultsp4pr.log
wFile=warmupp4pr.log
if [ -f "$file" ] && [ -f "$wFile" ]
then
  rm $file $wFile
fi
touch $file $wFile
echo "start warmup"
./warmup.sh >> $wFile # warmup
echo "end warmup"
for season in 1 2 3; do
  echo "start season $season"
  for benchcase in omp; do
    for i in `seq 1 1 10`;
    do
      # printf "$i:" >> $file # iteration
      ./build/video_task_omp 8 4 >> $file # results dumped
      sleep 1
    done
  done
  echo "end season $season"
done
