#!/usr/bin/env bash
file=resultsp5.log
wFile=warmupp5.log
if [ -f "$file" ] && [ -f "$wFile" ]
then
  rm $file $wFile
fi
touch $file $wFile
echo "start warmup"
./warmup.sh >> $wFile # warmup
echo "end warmup"
for season in 1 2 3; do
  echo "start season $season"
  for benchcase in seq omp; do
    echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      # printf "$i:" >> $file # iteration
      ./benchsuite $benchcase >> $file # results dumped
      sleep 1
    done
  done
  echo "end season $season"
done
