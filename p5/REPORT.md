# P5: Fractal de Mandelbrot

#### Carlos Labrador Viñuela

#### Diciembre 2019

## Prefacio

Objetivos conseguidos

Paralelización de un programa de generación de un fractal.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.
~~~bash
$ inxi -F
System:    Host: dartz-HP Kernel: 5.0.0-36-generic x86_64 bits: 64 Desktop: Gnome 3.28.4
           Distro: Ubuntu 18.04.3 LTS
Machine:   Device: laptop System: HP product: HP ENVY Notebook v: Type1ProductConfigId serial: N/A
           Mobo: HP model: 80DF v: 87.39 serial: N/A UEFI: Insyde v: F.19 date: 09/15/2015
Battery    BAT1: charge: 18.4 Wh 50.9% condition: 36.1/45.0 Wh (80%)
CPU:       Dual core Intel Core i5-6200U (-MT-MCP-) cache: 3072 KB
           clock speeds: max: 2800 MHz 1: 500 MHz 2: 500 MHz 3: 500 MHz 4: 500 MHz
Graphics:  Card: Intel Skylake GT2 [HD Graphics 520]
           Display Server: x11 (X.Org 1.20.4 ) driver: i915 Resolution: 1920x1080@60.05hz
           OpenGL: renderer: Mesa DRI Intel HD Graphics 520 (Skylake GT2) version: 4.5 Mesa 19.0.8
Audio:     Card Intel Sunrise Point-LP HD Audio driver: snd_hda_intel
           Sound: Advanced Linux Sound Architecture v: k5.0.0-36-generic
Network:   Card: Intel Wireless 7265 driver: iwlwifi
           IF: wlp1s0 state: up mac: 4c:34:88:5b:6a:4e
Drives:    HDD Total Size: 128.0GB (9.3% used)
           ID-1: /dev/sda model: SAMSUNG_MZNLF128 size: 128.0GB
Partition: ID-1: / size: 19G used: 11G (62%) fs: ext4 dev: /dev/sda6
           ID-2: swap-1 size: 0.50GB used: 0.00GB (0%) fs: swap dev: /dev/sda5
RAID:      No RAID devices: /proc/mdstat, md_mod kernel module present
Sensors:   System Temperatures: cpu: 37.0C mobo: N/A
           Fan Speeds (in rpm): cpu: N/A
Info:      Processes: 260 Uptime: 37 min Memory: 1904.3/3840.8MB Client: Shell (bash) inxi: 2.3.56
$ lscpu
Arquitectura:                        x86_64
modo(s) de operación de las CPUs:    32-bit, 64-bit
Orden de los bytes:                  Little Endian
CPU(s):                              4
Lista de la(s) CPU(s) en línea:      0-3
Hilo(s) de procesamiento por núcleo: 2
Núcleo(s) por «socket»:              2
«Socket(s)»                          1
Modo(s) NUMA:                        1
ID de fabricante:                    GenuineIntel
Familia de CPU:                      6
Modelo:                              78
Nombre del modelo:                   Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz
Revisión:                            3
CPU MHz:                             2745.224
CPU MHz máx.:                        2800,0000
CPU MHz mín.:                        400,0000
BogoMIPS:                            4800.00
Virtualización:                      VT-x
Caché L1d:                           32K
Caché L1i:                           32K
Caché L2:                            256K
Caché L3:                            3072K
CPU(s) del nodo NUMA 0:              0-3
Indicadores:                         fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d
~~~

## 2. Diseño e Implementación del Software
Mediante compilación condicional, hemos paralelizado la zona crítica del código que genera el cuello de botella, en la que se encuentra la llamada a la función `explode`. Etiquetamos las variables a usar y establecemos por defecto 4 threads. Tras diversas pruebas, lo mejor ha sido poner como planificador dynamic, con tamaño de chunk 80 (podría ser menor, los tiempos son similares).

~~~c++
#pragma omp parallel private(i,j,x,y) shared(count,count_max,n,x_max,y_max,x_min,y_min) num_threads(4)
{
  #if DEBUG
  #pragma omp master
  {
    printf("omp_num_threads: %d\n", omp_get_num_threads());
  }
  #endif
  #pragma omp for nowait collapse(2) schedule(dynamic, 80)
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      x = ((double)(j) * x_max + (double)(n - j - 1) * x_min) / (double)(n - 1);

      y = ((double)(i)* y_max + (double)(n - i - 1) * y_min) / (double)(n - 1);

      count[i + j * n] = explode(x, y, count_max);
    }
  }
} //end parallel region
~~~

Se ha paralelizado también, como se pide en el apartado 3, la parte del código que colorea el pixel de 4 maneras diferentes. Aseguramos la exclusión mutua de la variable `c_max`. Con variables de preprocesador establecemos cual o cuales de estas implementaciones queremos ejecutar.

- Directivas OpenMP: Usando la directiva `critical`.
~~~c++
#if COLOR_OMP
  c_max = 0;
  #pragma omp for collapse(2)
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
      if (c_max < count[i + j * n]) {
        #pragma omp critical
        {
          c_max = count[i + j * n];
        }
      }
    }
  }
#endif
~~~

- Runtime: Usamos los locks de las funciones del runtime de OpenMP para asegurar la exclusión mutua.
~~~c++
  #if COLOR_RUNTIME
  omp_lock_t lock;
  omp_init_lock(&lock);
  c_max = 0;
  #pragma omp for collapse(2)
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
      if (c_max < count[i + j * n]) {
        omp_set_lock(&lock);
        c_max = count[i + j * n];
        omp_unset_lock(&lock);
      }
    }
  }
  omp_destroy_lock(&lock);
#endif
~~~

- Secuencial: Usando la directiva `single` nos aseguramos que solo un hilo realiza la tarea.
~~~c++
  #if COLOR_SEC
  tIni = omp_get_wtime();
  #pragma omp single
  {
    c_max = 0;
    for (j = 0; j < n; j++) {
      for (i = 0; i < n; i++) {
        if (c_max < count[i + j * n]) {
          c_max = count[i + j * n];
        }
      }
    }
  }
  #endif
~~~

- Variable privada: Mediante la directiva `reduction` hacemos que el valor de `c_max` sea el máximo de los calculados parcialmente por los hilos.
~~~c++
#if COLOR_PRIVATE_MAX
  c_max = 0;
  #pragma omp for reduction(max:c_max)
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
      if (c_max < count[i + j * n]) {
        c_max = count[i + j * n];
      }
    }
  }
  #endif
~~~

## 3. Metodología y desarrollo de las pruebas realizadas

Se usan varios scripts con el fin de probar el programa. Además de los scripts de ejemplo dados, algo modificados, se ha creado un nuevo script **warmup**, cuya finalidad es hacer el calentamiento, ejecutando el programa unas cuantas veces.

###### Condiciones de la ejecución de las pruebas:
  - Misma frecuencia fijada en todos los procesadores, en este caso, la máxima.
  - Los scripts se ejecutan en la bash, la cual es la única aplicación abierta durante la ejecución de las pruebas.
  - Los scripts lanzan varias veces las dos versiones del programa (secuencial, OMP) obteniendo sus tiempos de ejecución, en segundos.

Scripts usados para el benchmarking:

**benchmark.sh:**

```sh
#!/usr/bin/env bash
file=resultsp5.log
wFile=warmupp5.log
if [ -f "$file" ] && [ -f "$wFile" ]
then
  rm $file $wFile
fi
touch $file $wFile
echo "start warmup"
./warmup.sh >> $wFile # warmup
echo "end warmup"
for season in 1 2 3; do
  echo "start season $season"
  for benchcase in seq omp; do
    echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      # printf "$i:" >> $file # iteration
      ./benchsuite $benchcase >> $file # results dumped
      sleep 1
    done
  done
  echo "end season $season"
done
```

**benchsuite.sh:**

```sh
#!/usr/bin/env bash
case "$1" in
  seq)
    ./build/seq
    ;;
  omp)
    for nthreads in 1 2 4 8
    do
      echo "omp $nthreads"
      ./build/omp $nthreads
    done
    ;;
esac
```

**warmup.sh:**

```sh
#!/usr/bin/env bash
for i in `seq 1 1 3`; do
  ./build/seq
  ./build/omp
done
sleep 1
```

#### Análisis de los resultados

![imagen_1](images/imagen_1.png)

- Se ha modificado el tamaño de la variable `n` para facilitar el benchmarking a 2000.
- A la vista de los resultados, observamos que con 2 hilos casi obtenemos el speedup máximo pero con 4 hilos la mejora de rendimiento ya no es tanta, aún siendo significativa.

El speedup es calculado mediante la media de las ejecuciones realizadas.
  - seq = 4,64758403508772 s
  - omp
    - 1 thread = 4,6533842 s
    - 2 threads = 2,5095788 s
    - 4 threads = 1,66831783 s
    - 8 threads = 1,705578567 s

### Paralelización y exclusión mutua bloque `c_max`

![imagen_2](images/imagen_2.png)

- Por defecto, 4 threads.
- Medimos los tiempos de ejecución de cada forma de implementación.
- Se mide el speedup respecto de la implementación secuencial. Observamos que la mejor implementación es la de directivas de omp (`critical`).
  - single = 0,0816067 s
  - directivas = 0,016233967 s
  - runtime = 0,016233967 s
  - variable privada = 0,073527367 s

## 4. Discusión

El tiempo que se usa en el benchmarking es el **tiempo de ejecución de la ROI**, usando la función del runtime de omp `omp_get_wtime`.

### Profiling
Mediante profiling, se concluyó que el cuello de botella lo provocaban las llamadas a `explode`, así que esa es la parte que se debe optimizar.

![imagen_3](images/imagen_3.png)

#### Explicar las directivas de OpenMP usadas
Evitamos nombrar las más básicas.
- schedule(dynamic): Usamos el planificador dinámico para la paralelización del for.
- nowait: Los hilos no se sincronizan al acabar su trabajo.
- collapse: 'Desenrolla' los for anidados para una paralelización más óptima.
- critical: Garantiza exclusión mutua a su bloque de código .
- single/master: Solo un hilo realiza el bloque de código. Si es el master, lo realiza el hilo principal.
- reduction: Trabaja con los resultados de todos los hilos, dependiendo de la operación.

#### Etiquetar las variables
- shared: count,count_max,n,x_max,y_max,x_min,y_min
- private: i,j,x,y

#### Cómo se ha conseguido el paralelismo
Se ha paralelizado la llamada a `explode`. Al encontrarse dentro de dos for anidados, usamos la cláusula `collapse` para conseguir una mejor paralelización.
