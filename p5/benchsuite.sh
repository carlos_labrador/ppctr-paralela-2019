#!/usr/bin/env bash
case "$1" in
  seq)
    ./build/seq
    ;;
  omp)
    for nthreads in 1 2 4 8
    do
      echo "omp $nthreads"
      ./build/omp $nthreads
    done
    ;;
esac
