#ifndef P2_HPP
#define P2_HPP

#define ITER 600000000  //SEQ 10s
#define DEBUG 0
#define A 1.5
#define B 2.4
#define C 1.0

void sequential();
void cpp();
void omp();

#endif // P2_HPP
