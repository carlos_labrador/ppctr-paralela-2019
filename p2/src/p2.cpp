#include <stdio.h>
#include <math.h>
#include <sys/time.h>

#ifdef CPP
#include <thread>
#include <mutex>
#include <condition_variable>
#endif

#ifdef OMP
#include <omp.h>
#endif

#include "p2.hpp"

//Global variables
int numThreads;
double gap;
long iter;

//Variables to measure execution time
struct timeval tIni, tFin;
double tExe;

int main(int argc, char* argv[]){
  gettimeofday(&tIni, NULL);
  //Number of iterations from parameter
  if(argc > 1){
    iter = atol(argv[1]);
    if(iter < 1){
      fprintf(stderr, "error: invalid iterations\n");
      return -1;
    }
  //Default number of iterations (to make SEQ execution 10 s)
  }else{
    iter = ITER;
  }
  #if DEBUG
  printf("iter: %ld\n", iter);
  #endif

  #ifndef SEQ
  //Number of threads from parameter
  if(argc > 2){
    numThreads = atoi(argv[2]);
    if(numThreads < 1){
      fprintf(stderr, "error: invalid threads\n");
      return -1;
    }
  //Default number of threads
  }else{
    numThreads = atoi(getenv("THREADS"));
  }
  #endif

  gap = 1.0/(double)iter;

  //Sequential execution
  #ifdef SEQ
  #if DEBUG
  printf("sequential\n");
  #endif
  sequential();
  #endif

  //CPP execution
  #ifdef CPP
  #if DEBUG
  printf("cpp\n");
  #endif
  cpp();
  #endif

  //OMP execution
  #ifdef OMP
  #if DEBUG
  printf("omp\n");
  #endif
  omp();
  #endif

  gettimeofday(&tFin, NULL);
  tExe = (tFin.tv_sec-tIni.tv_sec)+(tFin.tv_usec-tIni.tv_usec)/1000000.0;
  printf("execution time: %lf\n", tExe);
  return 0;
}

#ifdef SEQ
void sequential(){
  double ellipse;
  for (int i=0; i<iter; i++) {
    ellipse += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
  }
  ellipse *= 2*gap/sqrt((4*A*C-B*B));
	printf("ellipse: %f \n", ellipse);
}
#endif

#ifdef CPP
int rest, iterThread;
bool restDone = false;
//Synchronization and communication
std::mutex g_m;

void worker_function(double *ellipse, int index){
  double result = 0;
  int iniRange, finRange;

  iniRange = index * iterThread;
  finRange = iniRange + iterThread;

  #if DEBUG
  printf("thread: %d range: %d-%d\n", index, iniRange, finRange);
  #endif

  for (int i=iniRange; i<finRange; i++) {
    result += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
  }

  #if DEBUG
  printf("thread: %d result: %f\n", index, result);
  #endif

  //Protecting shared access to ellipse
  {std::lock_guard<std::mutex> l(g_m);
  *ellipse += result;}

  //If there are iterations left (rest)
  if(rest!=0){
    //Only first thread finishing his job does the rest
    if(!restDone){
      restDone = true;
      result = 0;
      iniRange = numThreads * iterThread;
      finRange = iniRange + rest;

      #if DEBUG
      printf("thread: %d (rest) range: %d-%d\n", index, iniRange, finRange);
      #endif

      for (int i=iniRange; i<finRange; i++) {
        result += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
      }

      #if DEBUG
      printf("thread: %d (rest) result: %f\n", index, result);
      #endif

      //Protecting shared access to ellipse
      {std::lock_guard<std::mutex> l(g_m);
      *ellipse += result;}
    }
  }
}

void cpp(){
  iterThread = iter/numThreads;
  rest = iter % numThreads;

  #if DEBUG
  printf("iter per thread: %d\n", iterThread);
  printf("rest: %d\n", rest);
  #endif

  double ellipse;

  //Creating and initializing threads
  std::thread threads[numThreads];
  for(int i=0; i<numThreads; i++){

    #if DEBUG
    printf("initializing thread %d\n", i);
    #endif

    threads[i] = std::thread(worker_function, &ellipse, i);
  }

  //Joining threads
  for(int i=0; i<numThreads; i++){

    #if DEBUG
    printf("joining thread %d\n", i);
    #endif

    threads[i].join();
  }

  ellipse *= 2*gap/sqrt((4*A*C-B*B));

  printf("ellipse: %f \n", ellipse);
}
#endif

#ifdef OMP
void omp(){
  double ellipse, partial;
  int i, iniRange, finRange, iterThread, rest;
  bool restDone = false;
  iterThread = iter/numThreads;
  rest = iter % numThreads;
  omp_lock_t lock;
  omp_init_lock(&lock);
  #pragma omp parallel private(i, partial, iniRange, finRange) shared (ellipse, iterThread, gap, rest) num_threads(numThreads)
  {
    #if DEBUG
    #pragma omp master
    {
      printf("omp_num_threads: %d\n", omp_get_num_threads());
    }
    #endif

    iniRange = omp_get_thread_num()*iterThread;
    finRange = iniRange + iterThread-1;

    #if DEBUG
    printf("omp_thread_num: %d range %d-%d: \n", omp_get_thread_num(), iniRange, finRange);
    #endif

    for (i=iniRange; i<=finRange; i++) {
      partial += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
    }

    #if DEBUG
    printf("omp_thread_num: %d result: %f\n", omp_get_thread_num(), partial);
    #endif

    //if there are iterations left (rest)
    if(rest!=0){
      omp_set_lock(&lock);
      if(!restDone){
        restDone = true;

        iniRange = numThreads*iterThread;
        finRange = iniRange+rest-1;

        #if DEBUG
        printf("omp_thread_num: %d rest range %d-%d: \n", omp_get_thread_num(), iniRange, finRange);
        #endif

        for (i=iniRange; i<=finRange; i++) {
          partial += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
        }

        #if DEBUG
        printf("omp_thread_num: %d result + rest: %f\n", omp_get_thread_num(), partial);
        #endif
      }
      omp_unset_lock(&lock);
    }

    omp_set_lock(&lock);
    ellipse += partial;
    omp_unset_lock(&lock);
  }
  omp_destroy_lock(&lock);
  ellipse *= 2*gap/sqrt((4*A*C-B*B));
	printf("ellipse: %f \n", ellipse);
}
#endif
