# P2: Optimizing

#### Carlos Labrador Viñuela

#### Diciembre 2019

## Prefacio

Objetivos conseguidos

Paralelización de un programa secuencial de dos diferentes maneras, con hilos de C/C++ y con OMP, usando las directivas básicas.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.
~~~bash
$ inxi -F
System:    Host: dartz-HP Kernel: 5.0.0-36-generic x86_64 bits: 64 Desktop: Gnome 3.28.4
           Distro: Ubuntu 18.04.3 LTS
Machine:   Device: laptop System: HP product: HP ENVY Notebook v: Type1ProductConfigId serial: N/A
           Mobo: HP model: 80DF v: 87.39 serial: N/A UEFI: Insyde v: F.19 date: 09/15/2015
Battery    BAT1: charge: 18.4 Wh 50.9% condition: 36.1/45.0 Wh (80%)
CPU:       Dual core Intel Core i5-6200U (-MT-MCP-) cache: 3072 KB
           clock speeds: max: 2800 MHz 1: 500 MHz 2: 500 MHz 3: 500 MHz 4: 500 MHz
Graphics:  Card: Intel Skylake GT2 [HD Graphics 520]
           Display Server: x11 (X.Org 1.20.4 ) driver: i915 Resolution: 1920x1080@60.05hz
           OpenGL: renderer: Mesa DRI Intel HD Graphics 520 (Skylake GT2) version: 4.5 Mesa 19.0.8
Audio:     Card Intel Sunrise Point-LP HD Audio driver: snd_hda_intel
           Sound: Advanced Linux Sound Architecture v: k5.0.0-36-generic
Network:   Card: Intel Wireless 7265 driver: iwlwifi
           IF: wlp1s0 state: up mac: 4c:34:88:5b:6a:4e
Drives:    HDD Total Size: 128.0GB (9.3% used)
           ID-1: /dev/sda model: SAMSUNG_MZNLF128 size: 128.0GB
Partition: ID-1: / size: 19G used: 11G (62%) fs: ext4 dev: /dev/sda6
           ID-2: swap-1 size: 0.50GB used: 0.00GB (0%) fs: swap dev: /dev/sda5
RAID:      No RAID devices: /proc/mdstat, md_mod kernel module present
Sensors:   System Temperatures: cpu: 37.0C mobo: N/A
           Fan Speeds (in rpm): cpu: N/A
Info:      Processes: 260 Uptime: 37 min Memory: 1904.3/3840.8MB Client: Shell (bash) inxi: 2.3.56
$ lscpu
Arquitectura:                        x86_64
modo(s) de operación de las CPUs:    32-bit, 64-bit
Orden de los bytes:                  Little Endian
CPU(s):                              4
Lista de la(s) CPU(s) en línea:      0-3
Hilo(s) de procesamiento por núcleo: 2
Núcleo(s) por «socket»:              2
«Socket(s)»                          1
Modo(s) NUMA:                        1
ID de fabricante:                    GenuineIntel
Familia de CPU:                      6
Modelo:                              78
Nombre del modelo:                   Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz
Revisión:                            3
CPU MHz:                             2745.224
CPU MHz máx.:                        2800,0000
CPU MHz mín.:                        400,0000
BogoMIPS:                            4800.00
Virtualización:                      VT-x
Caché L1d:                           32K
Caché L1i:                           32K
Caché L2:                            256K
Caché L3:                            3072K
CPU(s) del nodo NUMA 0:              0-3
Indicadores:                         fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d
~~~

## 2. Diseño e Implementación del Software

### 5.1.1. Base
- Implementamos la CLI, la cual recibe como parámetros el número de iteraciones y, opcionalmente, el número de hilos si no nos encontramos en la versión secuencial.
- Alternativamente, se acepta el número de hilos como variable de entorno. Si se pasa también por parámetro, este será el número de hilos, independientemente del valor de la variable de entorno.
- Si no se pasa el número de iteraciones, por defecto se asignarán 600000000, ya que con ese valor la versión secuencial tarda 10 segundos en ejecutar, tal y como se pide para las pruebas.
- Implementación de la compilación condicional, distinguiendo entre las versiones secuencial, C++ y OMP, en el código del programa y en el Makefile.
- Se ha modificado el código secuencial dado, ya que había variables que no estaban siendo usadas y que el código podía ser más compacto y eficiente.

  ***
  ~~~c++
  #ifdef SEQ
  void sequential(){
    double ellipse;
    for (int i=0; i<iter; i++) {
      ellipse += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
    }
    ellipse *= 2*gap/sqrt((4*A*C-B*B));
    printf("ellipse: %f \n", ellipse);
  }
  #endif
  ~~~
  **Código final de la versión secuencial.**
  ***

### 5.1.2. C++
- Paralelizamos la función `seq()` con hilos de C++. El procedimiento es el mismo que en la sección base de la práctica 1. Se crean los hilos indicados, entre 1 y 12, y se reparte el trabajo equitativamente entre ellos. Si hay algo de trabajo restante, el primer hilo en acabar su trabajo será el que lo realice.
- La función `cpp()` se encarga de calcular el trabajo para cada hilo, crear, inicializar los hilos, sincronizarlos cuando terminen y realizar la operación final.

  ***
  ~~~c++
  void cpp(){
    iterThread = iter/numThreads;
    rest = iter % numThreads;
    double ellipse;
    std::thread threads[numThreads];

    for(int i=0; i<numThreads; i++){
      threads[i] = std::thread(worker_function, &ellipse, i);
    }

    for(int i=0; i<numThreads; i++){
      threads[i].join();
    }

    ellipse *= 2*gap/sqrt((4*A*C-B*B));

    printf("ellipse: %f \n", ellipse);
  }
  ~~~
  ***

- Los hilos ejecutarán la función `worker_function`, en la cual cada hilo realiza su parte del trabajo y actualiza el resultado total en exclusión mutua.
- Para garantizar la exclusión mutua utilizamos un mutex, que los hilos tomarán cuando tengan que actualizar el resultado total. Posteriormente, lo liberarán.
  ***
  ~~~c++
  void worker_function(double *ellipse, int index){
    double result = 0;
    int iniRange, finRange;

    iniRange = index * iterThread;
    finRange = iniRange + iterThread;

    for (int i=iniRange; i<finRange; i++) {
      result += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
    }

    {std::lock_guard<std::mutex> l(g_m);
    *ellipse += result;}

    if(rest!=0){
      if(!restDone){
        restDone = true;
        result = 0;

        iniRange = numThreads * iterThread;
        finRange = iniRange + rest;

        for (int i=iniRange; i<finRange; i++) {
          result += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
        }

        {std::lock_guard<std::mutex> l(g_m);
        *ellipse += result;}
      }
    }
  }
  ~~~
  ***

### 5.1.3. OpenMP
- Ahora paralelizamos la función `seq()` usando OpenMP, pero de manera mínima, en la función `omp()`. El reparto de trabajo para los hilos será manual, y es exactamente igual que en la implementación paralela en C/C++.
- Creamos la sección paralela, equivalente a la función `worker_function` de la implementación en C/C++.
- El número de hilos nos lo asignará automáticamente OpenMP.
- Para garantizar la exclusión mutua al actualizar el resultado, utilizaremos los locks de OpenMP. También lo utilizaremos para hacer que sea el primer hilo en acabar su parte quien haga el resto. Se usan los locks ya que no estaba permitido usar directivas como critical o single, que normalmente son las que se utilizarían par estos dos casos.
- Al acabar la región paralela, se hará la última operación y acabará la función.

  ***
  ~~~c++
  #ifdef OMP
  void omp(){
    double ellipse, partial;
    int i, iniRange, finRange, iterThread, rest;
    bool restDone = false;
    iterThread = iter/numThreads;
    rest = iter % numThreads;
    omp_lock_t lock;
    omp_init_lock(&lock);
    #pragma omp parallel private(i, partial, iniRange, finRange) shared (ellipse, iterThread, gap, rest) num_threads(numThreads)
    {
      iniRange = omp_get_thread_num()*iterThread;
      finRange = iniRange + iterThread-1;

      for (i=iniRange; i<=finRange; i++) {
        partial += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
      }

      if(rest!=0){
        omp_set_lock(&lock);
        if(!restDone){
          restDone = true;
          iniRange = numThreads*iterThread;
          finRange = iniRange+rest-1;

          for (i=iniRange; i<=finRange; i++) {
            partial += 4/(1+((i+0.5)*gap)*((i+0.5)*gap));
          }
        }
        omp_unset_lock(&lock);
      }

      omp_set_lock(&lock);
      ellipse += partial;
      omp_unset_lock(&lock);
    }
    omp_destroy_lock(&lock);
    ellipse *= 2*gap/sqrt((4*A*C-B*B));
    printf("ellipse: %f \n", ellipse);
  }
  #endif
  ~~~
  ***

Las 3 funciones calculan correctamente el resultado final, con las suficientes iteraciones. `ellipse: 12.825498`

## 3. Metodología y desarrollo de las pruebas realizadas

Se usan varios scripts con el fin de probar el programa. Además de los scripts de ejemplo dados, algo modificados, se ha creado un nuevo script **warmup**, cuya finalidad es hacer el calentamiento, ejecutando el programa unas cuantas veces. Guardamos los resultados del calentamiento en otro archivo, por si se desea comprobarlos. Los resultados de la prueba son guardados en un archivo.

###### Condiciones de la ejecución de las pruebas:
  - Misma frecuencia fijada en todos los procesadores, en este caso, la máxima.
  - Los scripts se ejecutan en la bash, la cual es la única aplicación abierta durante la ejecución de las pruebas.
  - Los scripts lanzan varias veces las tres versiones del programa (secuencial, C/C++, OMP) obteniendo sus tiempos de ejecución, en segundos.

Scripts usados para el benchmarking:

**benchmark.sh:**

```sh
#!/usr/bin/env bash
file=resultsp2.log
wFile=warmupp2.log
if [ -f "$file" ] && [ -f "$wFile" ]
then
  rm $file $wFile
fi
touch $file $wFile
echo "start warmup"
./warmup.sh >> $wFile # warmup
echo "end warmup"
for season in 1 2; do
  echo "start season $season"
  for benchcase in seq cpp omp; do
    echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      # printf "$i:" >> $file # iteration
      ./benchsuite.sh $benchcase >> $file # results dumped
      sleep 1
    done
  done
  echo "end season $season"
done
```

**benchsuite.sh:**

```sh
#!/usr/bin/env bash
for nthreads in 1 2 4 8; do
  case "$1" in
    seq)
      ./build/seq
      ;;
    cpp)
      echo "cpp $nthreads"
      THREADS=$nthreads ./build/cpp
      ;;
    omp)
      echo "omp $nthreads"
      THREADS=$nthreads ./build/omp
      ;;
  esac
done
```

**warmup.sh:**

```sh
#!/usr/bin/env bash
iter=10000
nthreads=4
for i in `seq 1 1 100`; do
  ./build/seq $iter
  THREADS=$nthreads ./build/cpp $iter
  THREADS=$nthreads ./build/omp $iter
done
sleep 1
```

#### Análisis de los resultados

![imagen_1](images/imagen_1.png)

- Como ya se ha comentado antes, las pruebas se hacen con 600000000 iteraciones porque así el programa secuencial tarda 10 segundos.
- A la vista de los resultados, observamos que casi obtenemos el speedup máximo (dependiendo del número de hilos), y que el rendimiento de las versiones paralelas es prácticamente igual.

El speedup es calculado mediante la media de las ejecuciones realizadas.
  - seq = 9,921750725 s
  - cpp
    - 1 thread = 9,93689155 s
    - 2 threads = 5,14976095 s
    - 4 threads = 2,5914622 s
    - 8 threads = 2,5983757 s
  - omp
    - 1 thread = 9,934467 s
    - 2 threads = 5,14635145 s
    - 4 threads = 2,5923566 s
    - 8 threads = 2,605465 s

## 4. Discusión

Primero, tal y como pide el apartado **5.2.2.**, aclarar que el tiempo que se usa en el benchmarking es el **tiempo de ejecución total del programa**, usando la api `gettimeofday`.

Como valoración de la práctica, decir que es una buena práctica como transición del paralelismo de C++ a OMP. Quizás se debería poder usar más directivas de OMP para evitar volver a hacer el reparto de trabajo manualmente, como en la práctica 1 o en la parte de C++ de esta práctica.
