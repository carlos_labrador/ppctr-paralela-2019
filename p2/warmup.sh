#!/usr/bin/env bash
iter=10000
nthreads=4
for i in `seq 1 1 100`; do
  ./build/seq $iter
  THREADS=$nthreads ./build/cpp $iter
  THREADS=$nthreads ./build/omp $iter
done
sleep 1
