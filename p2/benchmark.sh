#!/usr/bin/env bash
file=resultsp2.log
wFile=warmupp2.log
if [ -f "$file" ] && [ -f "$wFile" ]
then
  rm $file $wFile
fi
touch $file $wFile
echo "start warmup"
./warmup.sh >> $wFile # warmup
echo "end warmup"
for season in 1 2; do
  echo "start season $season"
  for benchcase in seq cpp omp; do
    echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      # printf "$i:" >> $file # iteration
      ./benchsuite.sh $benchcase >> $file # results dumped
      sleep 1
    done
  done
  echo "end season $season"
done
