#!/usr/bin/env bash
for nthreads in 1 2 4 8; do
  case "$1" in
    seq)
      ./build/seq
      ;;
    cpp)
      echo "cpp $nthreads"
      THREADS=$nthreads ./build/cpp
      ;;
    omp)
      echo "omp $nthreads"
      THREADS=$nthreads ./build/omp
      ;;
  esac
done
