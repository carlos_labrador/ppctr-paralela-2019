#!/usr/bin/env bash
size=10000000
size2=90000000
op=xor
nthreads=8
case "$1" in
  st1)
    java -classpath src/ P1_base $size $op
    ;;
  mt1)
    java -classpath src/ P1_base $size $op --multi-thread $nthreads
    ;;
  st2)
    java -classpath src/ P1_base $size2 $op
    ;;
  mt2)
    java -classpath src/ P1_base $size2 $op --multi-thread $nthreads
    ;;
esac
