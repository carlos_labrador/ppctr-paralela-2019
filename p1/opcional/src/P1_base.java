//package practica01;

public class P1_base{

	//Constantes
	public static final int SUM = 0;
	public static final int SUB = 1;
	public static final int XOR = 2;
	public static final int MAXTHREADS = 12;
	//Debug flag
	public static boolean DEBUG = false;
	//Resultado final
	public static double resultado = 0;
	//Datos para ejecucion multithread
	public static Datos datos;

	public static void main(String[] args){

		//Comienzo ejecucion
		long tIni = System.nanoTime();

		if(args.length < 2) {
			System.out.println("Faltan parametros");
			System.exit(-1);
		}

		//Establecemos el numero de elementos
		int numEle = Integer.parseInt(args[0]);
		if(numEle < 2) {
			System.out.println("Numero de elementos insuficiente");
			System.exit(-1);
		}

		if(DEBUG) System.out.println("numEle: "+numEle);

		//Establecemos la operacion
		String strOp = args[1];
		int op = -1;
		if(strOp.equals("sum")) {
			op=SUM;
		}else if(strOp.equals("sub")) {
			op=SUB;
		}else if(strOp.equals("xor")) {
			op=XOR;
		}else{
			System.out.println("Operacion incorrecta");
			System.exit(-1);
		}

		if(DEBUG) System.out.println("op: "+op+" "+strOp);

		//Creamos la lista de valores
		double[] valores = new double[numEle];
		for(int i=0; i<numEle; i++) {
			valores[i] = i;
		}

		//Secuencial
		if(args.length < 4) {

			for(int i=0; i<numEle; i++) {
				resultado = hazOperacion(resultado, valores[i], op);
			}

	    if(DEBUG) System.out.printf("resultado: %.0f\n", resultado);
		//Multithreading
		}else{

			if(!args[2].equals("--multi-thread")) {
				System.out.println("Parametros incorrectos");
				System.exit(-1);
			}

			datos = new Datos();
			datos.numThreads = Integer.parseInt(args[3]);
			if(datos.numThreads < 1 || datos.numThreads > MAXTHREADS) {
				System.out.println("Numero de threads incorrecto");
				System.exit(-1);
			}

			if(DEBUG) System.out.println("numThreads: "+datos.numThreads);

			datos.numEle = numEle;
			datos.op = op;
			datos.trabajoPorThread = numEle/datos.numThreads;
			datos.resto = numEle % datos.numThreads;
			if(datos.resto!=0) {
				datos.hayResto = true;
			}

			if(DEBUG) System.out.println("trabajoPorThread: "+datos.trabajoPorThread);
			if(DEBUG) System.out.println("resto: "+datos.resto);

			//Creamos e inicializamos hilos
			Worker[] threads = new Worker[datos.numThreads];
			for(int i=0; i<datos.numThreads; i++) {
				threads[i] = new Worker(valores, i, datos);
				threads[i].start();
			}

			for(int i=0; i<datos.numThreads; i++) {
				try {
					threads[i].join();
				}catch(InterruptedException e) {
					return;
				}
			}

	    if(DEBUG) System.out.printf("resultado: %.0f\n", resultado);
		}
		//Pintamos tiempo de ejecucion
		System.out.println((System.nanoTime()-tIni)/1000000000.0);	//seg
		System.exit(0);
	}

	/**
	 * Opera dos numeros pasados como parametro
	 * @param x
	 * @param y
	 * @param op - SUM, SUB, XOR
	 * @return resul
	 */
	private static double hazOperacion(double x, double y, int op){
	  double resul = 0;
	  //sum
	  if(op==SUM)
	    resul = x+y;
	  //sub
	  else if(op==SUB)
	    resul = x-y;
	  //xor
	  else if(op==XOR)
	    resul = (int)x ^ (int)y;
	  return resul;
	}

	/**
	 * Hilos trabajadores
	 */
	public static class Worker extends Thread{

		private double[] valores;
		private int index;
		private Datos datos;

		public Worker(double[] valores, int index, Datos datos) {
			this.valores=valores;
			this.index=index;
			this.datos=datos;
		}

		@Override
		public void run() {
			double resul = 0;
			int iniRango = index * datos.trabajoPorThread;
			int finRango = iniRango + datos.trabajoPorThread-1;

			if(DEBUG) System.out.printf("thread %d rango %d-%d\n", index, iniRango, finRango);

			for(int i=iniRango; i<=finRango; i++) {
				resul = hazOperacion(resul, valores[i], datos.op);
			}

			if(DEBUG) System.out.printf("thread %d resultado %.0f\n", index, resul);

			synchronized(this) {
				resultado = hazOperacion(resultado, Math.abs(resul), datos.op);
			}

			if(datos.resto!=0) {
				if(datos.hayResto) {
					datos.hayResto = false;
					iniRango = datos.numThreads*datos.trabajoPorThread;
					finRango = iniRango+datos.resto-1;

					if(DEBUG) System.out.printf("thread %d rango %d-%d\n", index, iniRango, finRango);

					resul = 0;
					for(int i=iniRango; i<=finRango; i++) {
						resul = hazOperacion(resul, valores[i], datos.op);
					}

					if(DEBUG) System.out.printf("thread %d resultado %.0f\n", index, resul);

					//Solo accede un hilo a este bloque de codigo
					synchronized(this) {
						resultado = hazOperacion(resultado, Math.abs(resul), datos.op);
					}
				}
			}
		}
	}

	/**
	 * Datos del problema
	 */
	public static class Datos{
		public int numEle, op, trabajoPorThread, resto, numThreads;
		public boolean hayResto;
	}
}
