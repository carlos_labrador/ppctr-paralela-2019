#!/usr/bin/env bash
echo "start warmup"
for i in `seq 1 1 100`; do
  java -classpath src/ P1_base 1000000 sum
  java -classpath src/ P1_base 1000000 sub --multi-thread 4
done
echo "end warmup"
sleep 1
