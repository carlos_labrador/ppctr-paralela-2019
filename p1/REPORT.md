# P1: Multithreading en C++

#### Carlos Labrador Viñuela

#### Diciembre 2019

## Prefacio

Objetivos conseguidos

Buen entendimiento del paralelismo y la sincronización entre hilos en C/C++.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.
~~~bash
$ inxi -F
System:    Host: dartz-HP Kernel: 5.0.0-36-generic x86_64 bits: 64 Desktop: Gnome 3.28.4
           Distro: Ubuntu 18.04.3 LTS
Machine:   Device: laptop System: HP product: HP ENVY Notebook v: Type1ProductConfigId serial: N/A
           Mobo: HP model: 80DF v: 87.39 serial: N/A UEFI: Insyde v: F.19 date: 09/15/2015
Battery    BAT1: charge: 18.4 Wh 50.9% condition: 36.1/45.0 Wh (80%)
CPU:       Dual core Intel Core i5-6200U (-MT-MCP-) cache: 3072 KB
           clock speeds: max: 2800 MHz 1: 500 MHz 2: 500 MHz 3: 500 MHz 4: 500 MHz
Graphics:  Card: Intel Skylake GT2 [HD Graphics 520]
           Display Server: x11 (X.Org 1.20.4 ) driver: i915 Resolution: 1920x1080@60.05hz
           OpenGL: renderer: Mesa DRI Intel HD Graphics 520 (Skylake GT2) version: 4.5 Mesa 19.0.8
Audio:     Card Intel Sunrise Point-LP HD Audio driver: snd_hda_intel
           Sound: Advanced Linux Sound Architecture v: k5.0.0-36-generic
Network:   Card: Intel Wireless 7265 driver: iwlwifi
           IF: wlp1s0 state: up mac: 4c:34:88:5b:6a:4e
Drives:    HDD Total Size: 128.0GB (9.3% used)
           ID-1: /dev/sda model: SAMSUNG_MZNLF128 size: 128.0GB
Partition: ID-1: / size: 19G used: 11G (62%) fs: ext4 dev: /dev/sda6
           ID-2: swap-1 size: 0.50GB used: 0.00GB (0%) fs: swap dev: /dev/sda5
RAID:      No RAID devices: /proc/mdstat, md_mod kernel module present
Sensors:   System Temperatures: cpu: 37.0C mobo: N/A
           Fan Speeds (in rpm): cpu: N/A
Info:      Processes: 260 Uptime: 37 min Memory: 1904.3/3840.8MB Client: Shell (bash) inxi: 2.3.56
$ lscpu
Arquitectura:                        x86_64
modo(s) de operación de las CPUs:    32-bit, 64-bit
Orden de los bytes:                  Little Endian
CPU(s):                              4
Lista de la(s) CPU(s) en línea:      0-3
Hilo(s) de procesamiento por núcleo: 2
Núcleo(s) por «socket»:              2
«Socket(s)»                          1
Modo(s) NUMA:                        1
ID de fabricante:                    GenuineIntel
Familia de CPU:                      6
Modelo:                              78
Nombre del modelo:                   Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz
Revisión:                            3
CPU MHz:                             500.048
CPU MHz máx.:                        2800,0000
CPU MHz mín.:                        400,0000
BogoMIPS:                            4800.00
Virtualización:                      VT-x
Caché L1d:                           32K
Caché L1i:                           32K
Caché L2:                            256K
Caché L3:                            3072K
CPU(s) del nodo NUMA 0:              0-3
Indicadores:                         fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d
~~~

## 2. Diseño e Implementación del Software

### 5.1.1. Base

 Podemos separar el programa en dos partes, la **single-threaded** y la **multi-threaded**. Ambas tienen en común el parseo de argumentos y la generación del array de valores, de longitud indicada por el usuario, sobre los que se harán las operaciones.
  - La CLI requiere el número de elementos, la operación (*SUM*,*SUB*,*XOR*) y, opcionalmente, el número de hilos para la ejecución multi-thread. Si no se proporciona este último parámetro, el programa realizará la ejecución secuencial. `usage: <numEle> <operation> --multi-thread [numThreads]`

  - Para la lista de valores reservamos espacio en el heap con la instruccion `malloc` dependiendo del número de elementos, y posteriormente lo llenaremos con los valores [0, número de elementos). Al final del programa nos aseguramos de liberar esta memoria con `free`.

  También, nada más empezar el programa y usando la función `gettimeofday`, obtenemos el tiempo actual para calcular al final el tiempo total de ejecución.

  Ahora, ya distinguimos las dos partes.
  - En la parte **secuencial** simplemente calculamos el resultado en un bucle que llama a la función `hazOperacion(x,y,op)` la cual retorna el resultado de x +/-/^ y. Pintamos el resultado, y finalizamos el programa calculando el tiempo de ejecución.

  ~~~c++
  double hazOperacion(double x, double y, int op){
      double resul;
      //sum
      if(op==SUM)
        resul = x+y;
      //sub
      else if(op==SUB)
        resul = x-y;
      //xor
      else if(op==XOR)
        resul = (int)x ^ (int)y;
      return resul;
  }
  ...
  double resultado = 0;
  ...
  for(i=0; i<numEle; i++)
      resultado = hazOperacion(resultado, valores[i], op);
  ~~~

  - La parte **multi-thread** es obviamente más compleja.
    - Se ha creado un struct "Datos", el cual almacena el número de elementos, de hilos, el trabajo de cada hilo, el trabajo restante (resto) y la operación, con la finalidad de facilitar el paso de estas variables a la `worker_function` de los hilos.

    - Se definen varias variables para gestionar la sincronización de los hilos. `resto_ready` indicará, en caso de que haya trabajo restante, que se tenga que hacer este trabajo. Se utiliza también el mutex `g_m` para gestionar la sincronización entre los hilos.

    - Se parsea el número de hilos y se calcula el trabajo para cada hilo (```numElementos/numThreads```) y el resto (```numElementos % numThreads```).

    - Creamos una lista con el número de hilos establecidos y los inicializamos. Ahora, cada hilo está ejecutando `worker_function`.

    - Calculamos el rango en el que trabajará cada hilo, y acto seguido en un bucle calculamos el resultado de la operación en ese rango.

      ~~~c++
      int i, iniRango, finRango;
      double resul = 0;

      iniRango = index * datos->trabajoPorThread;
      finRango = iniRango + datos->trabajoPorThread-1;

      for(i=iniRango; i<=finRango; i++) resul = hazOperacion(resul, valores[i], datos->op);
      ~~~

    - Una vez calculado el resultado, el hilo toma el mutex (o espera si ya está tomado) y actualiza el valor del resultado final. Una vez actualizado, lo libera y continúa.

      ~~~c++
      {std::lock_guard<std::mutex> ul(g_m);
      resultado = hazOperacion(resultado, abs(resul), datos->op);}
      ~~~

    - Comprobamos si hay resto. Si no lo hay, el hilo acaba su trabajo. En caso de haberlo, comprobamos si el trabajo restante ya está hecho. Así nos aseguramos que el primer hilo en acabar su trabajo base realiza esta acción, ya que nada más pasar la condición cambiamos el valor de `resto_ready` a false, y como acaba de salir de un mutex sabemos que nunca entrarán dos o más hilos a la vez en esta parte del código. Después, mismo proceso que en el trabajo base para actualizar el resultado total.

      ~~~c++
      resto_ready = false;

      iniRango = datos->numThreads*datos->trabajoPorThread;
      finRango = iniRango+datos->resto-1;

      resul=0;
      for(i=iniRango; i<=finRango; i++) resul = hazOperacion(resul, valores[i], datos->op);
      ~~~

    - Finalmente los hilos terminan su trabajo, los sincronizamos con `join`, pintamos el resultado por pantalla y calculamos el tiempo de ejecución total.


  Tanto la parte secuencial como la multi-thread calculan los resultados correctamente, habiendo probado con diferentes número de elementos y de hilos.

  ###### Ejemplos de ejecución secuencial, multi-thread con resto (3 hilos) y multi-thread sin resto (8)

  ~~~bash
  $ ./build/p1 100000000 sum
  resultado: 4999999950000000
  tiempo total: 1.529758 s
  $ ./build/p1 100000000 sum --multi-thread 3
  resultado: 4999999950000000
  tiempo total: 0.981249 s
  $ ./build/p1 100000000 sum --multi-thread 8
  resultado: 4999999950000000
  tiempo total: 0.881859 s
  $ ./build/p1 100000000 sub
  resultado: -4999999950000000
  tiempo total: 1.533400 s
  $ ./build/p1 100000000 sub --multi-thread 3
  resultado: -4999999950000000
  tiempo total: 0.975456 s
  $ ./build/p1 100000000 sub --multi-thread 8
  resultado: -4999999950000000
  tiempo total: 0.882075 s
  $ ./build/p1 100000000 xor
  resultado: 0
  tiempo total: 1.866602 s
  $ ./build/p1 100000000 xor --multi-thread 3
  resultado: 0
  tiempo total: 1.057007 s
  $ ./build/p1 100000000 xor --multi-thread 8
  resultado: 0
  tiempo total: 0.975634 s
  ~~~

### 5.1.2. Logger

  El logger es un nuevo hilo que se añade al programa y que se encarga de sumar los resultados parciales de los hilos trabajadores y pasar el resultado final al main. Para activar esta función, ponemos la variable de preprocesador `FEATURE_LOGGER` a 1.

  - Para este nuevo hilo necesitamos nuevas variables para la gestión de su sincronización. `indexLogger` es un entero que identifica el hilo trabajador. Para la sincronización tenemos el mutex `mLogger`, la variable condicional `condLogger` y los booleanos `ready`, que indicará cuándo el logger está listo para recibir un resultado, y `main_ready`, la cual indicará cuando el main tiene el resultado totalmente actualizado, ambas inicializadas a false. En el main crearemos también una variable para almacenar el resultado final del logger `finalLogger` y una variable condicional `condResulLogger` que dormirá al main hasta que el logger le pase el resultado final.

  ~~~c++
  #if FEATURE_LOGGER
  int indexLogger;
  double parcialLogger, resultadoLogger;
  std::thread logger;
  std::mutex mLogger;
  std::condition_variable condLogger;
  bool ready = false;
  bool main_ready = false;
  #endif
  ~~~

  - Creamos la función que ejecutará el logger, `logger_function`. En ella el logger calcula la cantidad de datos que va a recibir (uno por hilo, si hay resto una más), y en un bucle recibe esos resultados. Se duerme esperando a que un hilo le pase el dato y le despierte. El logger almacena ese dato (con el mutex tomado), notifica a todos los hilos que ya está listo para recibir otro dato y se duerme (liberando el mutex) hasta que otro hilo tome el mutex y lo despierte de nuevo.

  ~~~c++
  void logger_function(Datos *datos, double *resulFinal, std::condition_variable *cond){
    int numResultados = datos->numThreads;
    int i;

    if(datos->resto!=0)
      numResultados += 1;

    double resultados[numResultados];

    for(i=0; i<numResultados; i++) {
      std::unique_lock<std::mutex> ulk(mLogger);
      condLogger.wait(ulk, []{return ready;});
      ready = false;
      resultados[indexLogger] = hazOperacion(resultados[indexLogger],
        abs(parcialLogger), datos->op);
      condLogger.notify_all();
      ulk.unlock();
    }

    std::unique_lock<std::mutex> ulk(mLogger);
    for(i=0;i<numResultados; i++) * resulFinal = hazOperacion(*resulFinal, abs(resultados[i]), datos->op);
    main_ready = true;
    cond->notify_one();
    ulk.unlock();
  }
  ~~~

  - Así se ve desde la parte de los hilos. Cogen el mutex cuando el logger esté disponible para recibir un resultado, se lo pasan, notifican al logger que ya le han pasado su resultado, liberan el mutex y continúan con su trabajo.

  ~~~c++
  void worker_function(double * valores, Datos *datos, int index){

      ...

      #if FEATURE_LOGGER
      std::unique_lock<std::mutex> ulk(mLogger);
      condLogger.wait(ulk, []{return !ready;});
      ready = true;
      parcialLogger = resul;
      indexLogger = index;
      condLogger.notify_one();
      ulk.unlock();
      #endif

      ...

      if(datos->resto!=0){
        if(resto_ready){

          ...

          #if FEATURE_LOGGER
          std::unique_lock<std::mutex> ulk(mLogger);
          condLogger.wait(ulk, []{return !ready;});
          ready = true;
          parcialLogger = resul;
          indexLogger = index;
          condLogger.notify_one();
          ulk.unlock();
          #endif

          ...
        }
      }
    }
  ~~~
  - Finalmente, el main espera a que el logger calcule el resultado total a partir de todos los resultados parciales calculados por los threads. Lo recibe del logger y le sincroniza.

  ~~~c++
  #if FEATURE_LOGGER
  std::unique_lock<std::mutex> ulk(mLogger);
  condResulLogger.wait(ulk, []{return main_ready;});
  resultadoLogger = finalLogger;
  ulk.unlock();
  ...
  logger.join();
  #endif
  ~~~

Para la versión final del programa, utilizamos esta característica cuando queramos mostrar el resultado de la operación.

### 5.1.3. Optimización del paralelismo

Para esta parte, se ha decidido sustituir el mutex que gestiona la sincronización de lo hilos a la hora de actualizar la variable global que almacena el resultado final por una variable **atómica** global. Se crea la flag `FEATURE_OPTIMIZE` como variable de preprocesador. Cuando esté activa, el programa usará esta variable atómica en lugar de la sincronización con mutex.

~~~c++
#if FEATURE_OPTIMIZE
#include <atomic>
std::atomic<double> atom(0);
#endif
...

void worker_function(double * valores, Datos *datos, int index){
  ...
  #if FEATURE_OPTIMIZE
  atom = hazOperacion(atom, abs(resul), datos->op);
  #else
  {std::lock_guard<std::mutex> ul(g_m);
  resultado = hazOperacion(resultado, abs(resul), datos->op);}
  #endif
  ...
}
...

int main(int argc, char *argv[]){
  ...
  #if FEATURE_OPTIMIZE
  resultado = atom;
  //printf("resultado atomico: %.lf\n", resultado);
  #else
  //printf("resultado: %.lf\n", resultado);
  #endif
  ...
}
~~~

Las conclusiones de esta modificación están dadas más abajo, en la explicación de las gráficas.

### 5.1.5. Voluntaria. Implementación sección base en Java

Se ha implementado la parte base de la práctica (5.1.1) en Java, en el archivo P1_base.java, y se comparan los tiempos de ejecución en comparación con la implementación en C/C++.

- El programa se compone de tres clases. `P1_base` es la clase principal, que contiene el main, donde se ejecuta el programa. La clase estática `Worker`, que extiende a `Thread` y representa los hilos trabajadores. La clase estática `Datos`, con solamente atributos públicos, con la finalidad de emular el struct de C y poder acceder a los datos del problema fácilmente.
También está el método estático `hazOperacion`, igual que el implementado en C/C++.

- En la clase `Worker` se implementa solo el método `run` de la clase `Thread`, que será la operación que realizarán los hilos cuando comiencen. Se hace lo mismo que en la función `worker_function` de la implementación de C, incluida la gestión del resto. La única diferencia es la sincronización entre los hilos. En lugar de usar mutex como en C, metemos el código a ejecutar en exclusión mutua en un bloque `synchronized`.

  ~~~java
  public static class Worker extends Thread{
    ...
    synchronized(this) {
      resultado = hazOperacion(resultado, Math.abs(resul), datos.op);
    }
    ...
  }
  ~~~

- En el main se realiza el parseo de argumentos, y se distingue entre ejecución secuencial y multithread dependiendo de los argumentos, como en la implementación en C. En la multithread, creamos los hilos, los guardamos en un array de `Worker` y los iniciamos. Cuando acaban, les sincronizamos.

  ~~~java
  Worker[] threads = new Worker[datos.numThreads];
  for(int i=0; i<datos.numThreads; i++) {
    threads[i] = new Worker(valores, i, datos);
    threads[i].start();
  }

  for(int i=0; i<datos.numThreads; i++) {
    try {
      threads[i].join();
    }catch(InterruptedException e) {
      return;
    }
  }
  ~~~

- Utilizamos `System.nanoTime` para medir el tiempo de ejecución del programa.

- Creamos una flag booleana de debug, para mostrar cuando queramos información de la ejecución y el resultado de la operación.

## 3. Metodología y desarrollo de las pruebas realizadas

Se usan varios scripts con el fin de probar el programa. Además de los scripts de ejemplo dados, algo modificados, se ha creado un nuevo script **warmup**, cuya finalidad es hacer el calentamiento, ejecutando el programa unas cuantas veces. Guardamos los resultados del calentamiento en otro archivo, por si se desea comprobarlos. Los resultados de la prueba son guardados en un archivo.

###### Condiciones de la ejecución de las pruebas:
  - Misma frecuencia fijada en todos los procesadores, en este caso, la mínima.
  - Los scripts se ejecutan en la bash, la cual es la única aplicación abierta durante la ejecución de las pruebas.
  - El programa prueba la ejecución secuencial y la multi-thread sin logger, obteniendo así solamente el tiempo de ejecución del programa completo en segundos.

Scripts usados para el benchmarking:

**benchmark.sh:**

```sh
#!/usr/bin/env bash
file=resultsp1.log
wFile=warmupp1.log
if [ -f "$file" ] && [ -f "$wFile" ]
then
  rm $file $wFile
fi
touch $file $wFile
./warmup.sh >> $wFile # warmup
for season in 1 2; do
  for benchcase in st1 mt1 st2 mt2; do
    echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      # printf "$i:" >> $file # iteration
      ./benchsuite.sh $benchcase >> $file # results dumped
      sleep 1
    done
  done
done
```

**benchsuite.sh:**

```sh
#!/usr/bin/env bash
size=10000000
size2=100000000
op=xor
nthreads=8
case "$1" in
  st1)
    ./build/p1 $size $op
    ;;
  mt1)
    ./build/p1 $size $op --multi-thread $nthreads
    ;;
  st2)
    ./build/p1 $size2 $op
    ;;
  mt2)
    ./build/p1 $size2 $op --multi-thread $nthreads
    ;;
esac
```

**warmup.sh:**

```sh
#!/usr/bin/env bash
echo "start warmup"
for i in `seq 1 1 100`; do
  ./build/p1 1000000 sum
  ./build/p1 1000000 xor --multi-thread 4
done
echo "end warmup"
sleep 1
```

#### Análisis de los resultados
###### Parte base
Se han realizado pruebas con dos diferentes números de elementos, 10 millones y 100 millones. No se han podido hacer de más tamaño debido a la memoria.
- En la siguiente gráfica comparamos el speedup de las ejecuciones multithread con 1, 2, 4 y 8 threads respecto de la secuencial con un array de 10 millones de elementos. Los speedup son calculados a partir de la media de los resultados obtenidos.
  - st1 = 0,1796012875 s
  - mt1(1) = 0,1803762 s
  - mt1(2) = 0,12282805 s
  - mt1(4) = 0,0941581 s
  - mt1(8) = 0,09641565 s

    ![Gráfica 1](images/grafica_01.png)

- En la siguiente gráfica comparamos el speedup de las ejecuciones multithread con 1, 2, 4 y 8 threads respecto de la secuencial con un array de 100 millones de elementos.
  - st2 = 1,762416025 s
  - mt2(1) = 1,76303785 s
  - mt2(2) = 1,16786475 s
  - mt2(4) = 0,9139894 s
  - mt2(8) = 0,9166429 s

    ![Gráfica 2](images/grafica_02.png)

###### Conclusiones
Se puede observar que la manera más eficiente de ejecutar el programa es con multithread de 4 hilos, debido a los 4 cores del sistema. A partir de los 4, empieza a castigar el overhead. Aunque no se aprecia mucho, la ejecución multithread de 1 thread es algo peor que la secuencial, algo lógico. Observamos también que el rendimiento mejora proporcionalmente con los hilos, hasta que llegamos al 'tope' de 4. Por último, se observa que cuanto más elementos haya mejor se aprovecha el paralelismo y más mejora el rendimiento de un programa.

###### FEATURE_OPTIMIZE
Se han realizado las mismas pruebas para la parte base. Se calculan los speedup respectdo de la parte secuencia y respecto de la parte multithread.
- En la siguiente gráfica comparamos el speedup de las ejecuciones multithread con `FEATURE_OPTIMIZE` con 1, 2, 4 y 8 threads respecto de la secuencial con un array de 10 millones de elementos. Los speedup son calculados a partir de la media de los resultados obtenidos.
  - st1 = 0,1786292 s
  - mt1(1) = 0,1807746 s
  - mt1(2) = 0,12441785 s
  - mt1(4) = 0,0945967 s
  - mt1(8) = 0,0964668 s

    ![Gráfica 3](images/grafica_03.png)

- En la siguiente gráfica comparamos el speedup de las ejecuciones multithread con `FEATURE_OPTIMIZE` con 1, 2, 4 y 8 threads respecto de la secuencial con un array de 100 millones de elementos.
  - st2 = 1,753996075 s
  - mt2(1) = 1,75397905 s
  - mt2(2) = 1,15915525 s
  - mt2(4) = 0,8811308 s
  - mt2(8) = 0,89189085 s

    ![Gráfica 4](images/grafica_04.png)

- En la siguiente gráfica comparamos el speedup de las ejecuciones multithread con `FEATURE_OPTIMIZE` con 1, 2, 4 y 8 threads respecto de la multithread sin `FEATURE_OPTIMIZE` con los mismos thread y con un array de 10 millones de elementos.

    ![Gráfica 5](images/grafica_05.png)

- En la siguiente gráfica comparamos el speedup de las ejecuciones multithread con `FEATURE_OPTIMIZE` con 1, 2, 4 y 8 threads respecto de la multithread sin `FEATURE_OPTIMIZE` con los mismos thread y con un array de 100 millones de elementos.

    ![Gráfica 6](images/grafica_06.png)

###### Conclusiones
Con las dos últimas gráficas se observa claramente que cuantos más elementos haya, más se mejora el programa con la optimización propuesta. Si no hay tantos elementos, el rendimiento es mínimamente peor, prácticamente igual a la ejecución multithread con mutex.

###### Parte optativa
Se han modificado algo los scripts anteriores para probarlo con la implementación en java de la parte base. Se prueba con dos números de elementos diferentes, 10 millones y 90 millones. No se puede probar con 100 millones por memoria del jre, así que no se pueden comparar las ejecuciones multithread de C de 100 millones. Las dos primeras comparaciones son de la propia implementación en java, la tercera es la comparación con la implementación de C/C++.

- En las siguientes gráficas vemos la comparación del speedup de la parte multithread con la secuencial con 10 y 90 millones de elementos respectivamente. Como es obvio, el paralelismo no está bien implementado, ya que no mejora la versión secuencial. Sin tiempo para corregirlo, seguramente el error sea el bloque `synchronized`, ya que de alguna manera 'bloquea' de más el thread. Se plantea la solución de sustituir el bloque `synchronized` por un mutex, como en la implementación en C/C++.
Aún así, con un gran número de elementos parece que a la larga mejora, habría que hacer más pruebas con más elementos, pero no es posible.

    ![Gráfica 7](images/grafica_07.png)

    ![Gráfica 8](images/grafica_08.png)

- Finalmente, comparamos las implementaciones en Java y en C/C++. Podemos observar que tal y como está implementado, cuanto menos paralelismo haya más eficiente es la implementación en Java. Se puede observar una clara mejora en la ejecución secuencial, con un speedup de 2,178 de media, y que a medida que hay más paralelismo el rendimiento va cayendo. Seguramente se deba a que esté mal implementado el paralelismo actual.

    ![Gráfica 9](images/grafica_09.png)

Todos los datos de los resultados con los que se han obtenido estas gráficas y conclusiones se encuentran en el repositorio p1 en el archivo benchmarking.ods.

## 4. Discusión

Primero, tal y como pide el apartado **5.2.2.**, aclarar que el tiempo que se usa en el benchmarking es el **tiempo de ejecución total del programa**, usando la api `gettimeofday`.

Aclarar, por supuesto, que las operaciones dan su resultado correctamente independientemente del número de hilos, como ya se comprueba en la explicación de la parte base en el punto 2.

Como valoración de la práctica, decir que es una práctica dura para ser la primera de la asignatura, pero que en complemento con la práctica 2 te lleva a entender bastante bien la sincronización y el paralelismo en C/C++. El logger tiene su dificultad, ya que exige un extra de sincronización que al principio puede liar fácilmente.

La parte del conector con Java no está hecha, ya que personalmente no tenía claro dónde y cómo había que ordenar y situar el código para que funcionase. Si tuviera que cambiar algo de la práctica, sería esta parte, aunque entiendo su finalidad para medir cómo influye en el rendimiento el overhead que nos mete la parte de java.

Finalmente, comentar que se detectó el fallo en el paralelismo en la parte opcional al final, sin tiempo para implementar una solución, aunque como ya he comentado antes, seguramente el fallo sea usar el bloque `synchronized` como mecanismo de exclusión mutua.
