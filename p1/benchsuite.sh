#!/usr/bin/env bash
size=10000000
size2=100000000
op=xor
nthreads=8
case "$1" in
  st1)
    ./build/p1 $size $op
    ;;
  mt1)
    ./build/p1 $size $op --multi-thread $nthreads
    ;;
  st2)
    ./build/p1 $size2 $op
    ;;
  mt2)
    ./build/p1 $size2 $op --multi-thread $nthreads
    ;;
esac
