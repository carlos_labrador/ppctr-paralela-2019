#!/usr/bin/env bash
file=resultsp1.log
wFile=warmupp1.log
if [ -f "$file" ] && [ -f "$wFile" ]
then
  rm $file $wFile
fi
touch $file $wFile
./warmup.sh >> $wFile # warmup
for season in 1 2; do
  for benchcase in st1 mt1 st2 mt2; do
    echo $benchcase >> $file
    for i in `seq 1 1 10`;
    do
      # printf "$i:" >> $file # iteration
      ./benchsuite.sh $benchcase >> $file # results dumped
      sleep 1
    done
  done
done
