#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "constantes.hpp"

#if FEATURE_OPTIMIZE
#include <atomic>
std::atomic<double> atom(0);
#endif

//Struct datos del problema
typedef struct Datos{
  int numElementos, numThreads, trabajoPorThread, resto, op;
}Datos;

//Variables
bool resto_ready = true;  //si tiene que hacer el trabajo restante
std::mutex g_m; //gestiona acceso al resultado

//Variables logger
#if FEATURE_LOGGER
int indexLogger;  //thread index
double parcialLogger, resultadoLogger;
std::thread logger;
std::mutex mLogger;
std::condition_variable condLogger;
bool ready = false; //si esta listo para recibir resultados
bool main_ready = false;  //si está listo para pasar resultado a main
#endif

double resultado = 0; //resultado total

//Variables para medir tiempos
struct timeval tIni,tFin, tIniThread, tFinThread;
double tiempo, tiempoThreads;

//Prototipos
double hazOperacion(double, double, int);
#if FEATURE_LOGGER
void logger_function(Datos *, double *, std::condition_variable *);
#endif
void worker_function(double *, Datos *, int);

/*
 * Programa principal
 */
int main(int argc, char *argv[]){
  gettimeofday(&tIni, NULL);
  int numEle, op, i;

  if(argc < 2){
    fprintf(stderr, "Faltan argumentos\n");
    return -1;
  }

  //Establecemos el numero de elementos
  numEle = atoi(argv[1]);
  if(numEle < 2){
    fprintf(stderr, "Numero de elementos incorrecto\n");
    return -1;
  }

  #if DEBUG
  printf("numElementos: %d\n", numEle);
  #endif

  if(argc < 3){
    fprintf(stderr, "Faltan argumentos\n");
    return -1;
  }

  //Establecemos la operacion a realizar
  if(numEle < 2){
    fprintf(stderr, "Operacion incorrecta\n");
    return -1;
  }

  char* operacion = argv[2];
  if(strcmp(operacion,"sum")==0)
    op=SUM;
  else if(strcmp(operacion,"sub")==0)
    op=SUB;
  else if(strcmp(operacion,"xor")==0)
    op=XOR;
  else{
    fprintf(stderr, "Operacion incorrecta\n");
    return -1;
  }

  #if DEBUG
  printf("op: %d, %s\n", op, operacion);
  #endif

  //Reservamos memoria para el array de elementos
  double* valores = (double *) malloc(numEle * sizeof(double));
  //Llenamos el array de elementos
  for(i=0; i<numEle; i++) valores[i]=i;

  #if DEBUG
  printf("valores: ");
  for(i=0; i<numEle; i++) printf("%.lf ", valores[i]);
  printf("\n");
  #endif

  //Secuencial
  if(argc < 4){
    for(i=0; i<numEle; i++) resultado = hazOperacion(resultado, valores[i], op);
    //printf("resultado: %.lf\n", resultado);
  //Multithreading
  }else{
    Datos datos;

    if(strcmp(argv[3],"--multi-thread")!=0){
      fprintf(stderr, "Parametros incorrectos\n");
      return -1;
    }

    datos.numThreads = atoi(argv[4]);
    if(datos.numThreads < 1 || datos.numThreads > MAXTHREADS){
      fprintf(stderr, "Numero de threads incorrecto\n");
      return -1;
    }

    #if DEBUG
    printf("numThreads: %d\n", datos.numThreads);
    #endif

    datos.numElementos = numEle;
    datos.op = op;

    //Definimos la carga de trabajo por thread
    datos.trabajoPorThread = datos.numElementos/datos.numThreads;
    datos.resto = datos.numElementos % datos.numThreads;

    #if DEBUG
    printf("trabajoPorThread: %d\n", datos.trabajoPorThread);
    printf("resto: %d\n", datos.resto);
    #endif

    //Creamos logger y las variables para comunicarse con el main
    #if FEATURE_LOGGER
    double finalLogger=0;
    #if DEBUG
    printf("Creando logger\n");
    #endif
    std::condition_variable condResulLogger;
    logger = std::thread(logger_function, &datos, &finalLogger, &condResulLogger);
    #endif

    //Creamos hilos
    std::thread threads[datos.numThreads];
    for(i=0; i<datos.numThreads; i++){

      #if DEBUG
      printf("Creando thread %d\n", i);
      #endif

      threads[i] = std::thread(worker_function, valores, &datos, i);
    }

    //Resultado logger
    #if FEATURE_LOGGER
    std::unique_lock<std::mutex> ulk(mLogger);
    condResulLogger.wait(ulk, []{return main_ready;});
    resultadoLogger = finalLogger;
    printf("resultadoLogger: %.lf\n", resultadoLogger);
    ulk.unlock();
    #endif

    //Sincronizamos los hilos
    for(i=0; i<datos.numThreads; i++){

      #if DEBUG
      printf("Join thread %d\n", i);
      #endif

      threads[i].join();
    }

    #if DEBUG
    printf("Join logger\n");
    #endif

    //Sincronizamos el logger
    #if FEATURE_LOGGER
    logger.join();
    #endif

    #if FEATURE_OPTIMIZE
    resultado = atom;
    //printf("resultado atomico: %.lf\n", resultado);
    #else
    //printf("resultado: %.lf\n", resultado);
    #endif
  }
  free(valores);
  gettimeofday(&tFin, NULL);
  tiempo = (tFin.tv_sec-tIni.tv_sec)+(tFin.tv_usec-tIni.tv_usec)/1000000.0;
  printf("%lf\n", tiempo);

  #if FEATURE_LOGGER
  return !(resultadoLogger==resultado); //0 si es correcto
  #endif

  return 0;
}

/*
 * Opera con dos numeros pasados como parametro
 */
double hazOperacion(double x, double y, int op){
  double resul;
  //sum
  if(op==SUM)
    resul = x+y;
  //sub
  else if(op==SUB)
    resul = x-y;
  //xor
  else if(op==XOR)
    resul = (int)x ^ (int)y;
  return resul;
}

#if FEATURE_LOGGER
/*
 * Tarea que ejecuta el logger
 */
void logger_function(Datos *datos, double *resulFinal, std::condition_variable *cond){
  int numResultados = datos->numThreads;
  int i;

  if(datos->resto!=0)
    numResultados += 1;

  double resultados[numResultados];

  //Mientras no tenga todos los resultados
  for(i=0; i<numResultados; i++) {
    //Dormir hasta resultado de thread listo
    std::unique_lock<std::mutex> ulk(mLogger);
    condLogger.wait(ulk, []{return ready;});
    //Despierta y recibe el resultado de un thread y su indice
    ready = false;
    resultados[indexLogger] = hazOperacion(resultados[indexLogger],
      abs(parcialLogger), datos->op);

    #if DEBUG
    printf("thread logger resultado %.lf thread %d\n", parcialLogger, indexLogger);
    #endif

    condLogger.notify_all();
    ulk.unlock();
  }

  //Pasa los resultados al main
  std::unique_lock<std::mutex> ulk(mLogger);
  //Suma todos los resultados
  for(i=0;i<numResultados; i++) *resulFinal = hazOperacion(*resulFinal, abs(resultados[i]), datos->op);
  main_ready = true;
  cond->notify_one();
  ulk.unlock();
}
#endif

/*
 * Tarea que ejecutan los threads
 */
void worker_function(double * valores, Datos *datos, int index){
  int i, iniRango, finRango;
  double resul = 0;
  iniRango = index * datos->trabajoPorThread;
  finRango = iniRango + datos->trabajoPorThread-1;

  #if DEBUG
  printf("thread %d rango %d-%d\n", index, iniRango, finRango);
  #endif

  for(i=iniRango; i<=finRango; i++) resul = hazOperacion(resul, valores[i], datos->op);

  #if DEBUG
  printf("thread %d resultado %.lf\n", index, resul);
  #endif

  //Pasar resultado
  #if FEATURE_LOGGER
  //despertar logger y mandarle index y resul
  std::unique_lock<std::mutex> ulk(mLogger);
  condLogger.wait(ulk, []{return !ready;});
  ready = true;
  parcialLogger = resul;

  #if DEBUG
  printf("thread %d parcialLogger %.lf\n", index, parcialLogger);
  #endif

  indexLogger = index;
  condLogger.notify_one();
  ulk.unlock();
  #endif

  #if FEATURE_OPTIMIZE
  atom = hazOperacion(atom, abs(resul), datos->op);
  #else
  {std::lock_guard<std::mutex> ul(g_m);
  resultado = hazOperacion(resultado, abs(resul), datos->op);}
  #endif

  //Si queda algo de trabajo y es el primero en acabar, lo realiza
  if(datos->resto!=0){
    if(resto_ready){
      resto_ready = false;

      #if DEBUG
      printf("thread %d lock mutex\n", index);
      #endif

      iniRango = datos->numThreads*datos->trabajoPorThread;
      finRango = iniRango+datos->resto-1;

      #if DEBUG
      printf("thread %d rango %d-%d\n", index, iniRango, finRango);
      #endif

      resul=0;
      for(i=iniRango; i<=finRango; i++) resul = hazOperacion(resul, valores[i], datos->op);

      #if DEBUG
      printf("thread %d resultado %.lf\n", index, resul);
      #endif

      //Pasar resultado
      #if FEATURE_LOGGER
      //despertar logger y mandarle index y resul
      std::unique_lock<std::mutex> ulk(mLogger);
      condLogger.wait(ulk, []{return !ready;});
      ready = true;
      parcialLogger = resul;

      #if DEBUG
      printf("thread %d parcialLogger %.lf\n", index, parcialLogger);
      #endif

      indexLogger = index;
      condLogger.notify_one();
      ulk.unlock();
      #endif

      #if FEATURE_OPTIMIZE
      atom = hazOperacion(atom, abs(resul), datos->op);
      #else
      {std::lock_guard<std::mutex> ul(g_m);
      resultado = hazOperacion(resultado, abs(resul), datos->op);}
      #endif
    }
    //Acabamos, se libera el mutex
  }
  //El thread acaba el trabajo
}
